libjavascript-beautifier-perl (0.25-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Nov 2022 21:11:19 +0000

libjavascript-beautifier-perl (0.25-2) unstable; urgency=medium

  * Team upload
  * Add missing "=>" operator (ES6) (Closes: #931379)
  * Declare compliance with policy 4.3.0
  * Bump debhelper compatibility level to 12

 -- Xavier Guimard <yadd@debian.org>  Mon, 08 Jul 2019 06:56:47 +0200

libjavascript-beautifier-perl (0.25-1) unstable; urgency=medium

  * Import upstream version 0.25.
  * Refresh remove-pl-extension.patch.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.1.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 28 Jul 2018 16:34:18 +0200

libjavascript-beautifier-perl (0.21-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.21.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Mar 2018 19:11:58 +0200

libjavascript-beautifier-perl (0.20-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.20.
  * Drop pod.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Dec 2015 22:18:47 +0100

libjavascript-beautifier-perl (0.19-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.19.
  * Add a patch to fix a POD error.
  * Extend remove-pl-extension.patch: add new occurrences of the filename.
  * Bump debhelper compatibility level to 9.
  * Drop obsolete debian/README.source.
  * Add some packages to Recommends which are needed by the new
    Code::TidyAll::Plugin::JSBeautifier module.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Dec 2015 18:16:06 +0100

libjavascript-beautifier-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/rules: switch order of arguments to dh.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Switch to source format "3.0 (quilt)"
    + Remove "--with quilt" from dh call and quilt build-dependency.
    + Rename js_beautify* after being installed instead of in the source.
    + Remove Makefile.PL from debian/patches/remove-pl-extension.patch.
  * Bump debhelper compatibility to 8
    + Update versioned debhelper build-dependency accordingly.
    + Add explicit build dependency on libmodule-build-perl
  * Add debian/upstream/metadata
  * Import upstream version 0.18
    + Refresh patch.
  * Fix lintian warning copyright-refers-to-symlink-license.
  * Reindent debian/copyright.
  * Fix typo in copyright holder's e-mail address.

 -- Axel Beckert <abe@debian.org>  Tue, 09 Jun 2015 00:21:12 +0200

libjavascript-beautifier-perl (0.17-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Refresh patch to match new js_beautify.pl

  [ Ryan Niebur ]
  * Update ryan52's email address

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 30 Dec 2009 23:35:29 -0500

libjavascript-beautifier-perl (0.16-1) unstable; urgency=low

  * New upstream release
    + Fix array indentation regressions
  * Depend on perl >= 5.10.1 or libtest-simple-perl >= 0.88

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 22 Sep 2009 06:38:52 -0400

libjavascript-beautifier-perl (0.15-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Changes to handling of arrays

  [ Ryan Niebur ]
  * Update jawnsy's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 19 Sep 2009 20:39:48 -0400

libjavascript-beautifier-perl (0.14-1) unstable; urgency=low

  * New upstream release
  * Debian Policy 3.8.3

 -- Ryan Niebur <ryanryan52@gmail.com>  Wed, 19 Aug 2009 16:04:02 -0700

libjavascript-beautifier-perl (0.13-1) unstable; urgency=low

  * New upstream release (0.12)
    + Allow unescaped / in regular expression character classes
    + Fixed broken <!-- / --> handline
    + Added space between anonymous function parentheses
  * Standards-Version 3.8.2 (no changes)
  * Updated description in control file
  * Added myself to Uploaders and Copyright
  * Refreshed patch, renamed to remove-pl-extension.patch (Closes: #538676)
    + Patch the file before renaming, and unpatch after renaming, so that it
      works with the new quilt patch system

  * New upstream release (0.13)
    + Fix for prefix operators (increment,decrement)
    + Put spaces in front of colons in ternary (a ? b : c) expressions
    + No spaces in front of colons in class literals ({ 1: "a", 2: "b" })
    + Remove spaces between unary +/-/~ and the operand (-a, case -1, !a)
    + Add a blank line between a new block and the start of an expression

 -- Jonathan Yu <frequency@cpan.org>  Sun, 02 Aug 2009 06:41:58 -0400

libjavascript-beautifier-perl (0.11-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * New upstream release

 -- Ryan Niebur <ryanryan52@gmail.com>  Mon, 08 Jun 2009 00:21:33 -0700

libjavascript-beautifier-perl (0.09-1) unstable; urgency=low

  * Initial Release. (Closes: #531140)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 31 May 2009 19:59:01 -0700
